#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

while getopts ":p" opt; do
  case ${opt} in
    p ) DO_PULL=y
      ;;
    \? ) echo "Usage: setup.sh [-p]"
         exit 1
      ;;
  esac
done

# first create data directory for postgres
# we cannot provide this from the repo directly because git does not support
# empty directories. so to keep the directory in git it would need to contain
# an empty file. if the data directory is not empty postgres will not initialize
# the database though

mkdir -p "${DIR}/db/data"

# if desired we pull the newest versions of the images
if [ ! -z "${DO_PULL}" ]; then
    docker-compose pull
else
    echo "Warning: No new image versions have been pulled. If you want to pull new image version start this script with the \"-p\" argument"
fi

# then setup all docker containers using Compose
docker-compose up -d

# this will start only one celery worker node regardless of how many queues are needed
# so now we will query how many nodes are needed and then we start the necessary
# amount of additional workers
WORKER_COUNT=$(docker-compose exec -T yadg python manage.py celerycmdline nodes | wc -l)

if [ ! -z "${WORKER_COUNT}" ]; then
    docker-compose scale celery=${WORKER_COUNT}
else
    echo "Could not determine worker count"
    exit 1
fi

